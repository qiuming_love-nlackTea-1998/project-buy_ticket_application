package com.qiuming.alibaba.config;

import org.springframework.amqp.core.*;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

//rabbitMQ的配置
@Configuration
public class MQConfig {
    //定义交换机
    public static final String EXCHNAGE_DELAY = "ALIBABA_EXCHNAGE_DELAY";

    //定义订单队列
    public static final String QUEUE_ORDER = "ALIBABA_QUEUE_ORDER";
    //定义死信队列
    public static final String QUEUE_DELAY = "ALIBABA_QUEUE_DELAY";
    //定义支付队列
    public static final String QUEUE_PAY = "ALIBABA_QUEUE_PAY";
    //定义非监听队列，即用于延时消息的发送
    public static final String QUEUE_UNKNOW = "ALIBABA_QUEUE_UNKNOW";

    //定义订单队列路由键
    public static final String ROUTINGKEY_QUEUE_ORDER = "ALIBABA_ROUTINGKEY_QUEUE_ORDER";
    //定义死信队列路由键
    public static final String ROUTINGKEY_QUEUE_DELAY = "ALIBABA_ROUTINGKEY_QUEUE_DELAY";
    //定义支付队列路由键
    public static final String ROUTINGKEY_QUEUE_PAY = "ALIBABA_ROUTINGKEY_QUEUE_PAY";

    //定义非监听队列路邮件，即用户绑定延时消息
    public static final String ROUTINGKEY_QUEUE_UNKNOW = "ALIBABA_ROUTINGKEY_QUEUE_UNKNOW";

    //定义交换机
    @Bean
    public Exchange exchangeDelay(){
        return ExchangeBuilder.topicExchange(EXCHNAGE_DELAY).durable(true).build();
    }

    //该队列为订单队列
    @Bean(QUEUE_ORDER)
    public Queue queueOrder(){
        return new Queue(QUEUE_ORDER,true);
    }
    //该队列接收死信交换机转发过来的消息
    @Bean(QUEUE_DELAY)
    public Queue queueDelay(){
        return new Queue(QUEUE_DELAY,true);
    }
    //该队列为支付队列
    @Bean(QUEUE_PAY)
    public Queue queuePay(){
        return new Queue(QUEUE_PAY,true);
    }
    //该队列为延时队列，该队列中的消息需要设置ttl
    @Bean(QUEUE_UNKNOW)
    public Queue queueUnKnow(){
        Map<String,Object> map = new HashMap<>();
        //过期的消息给哪个交换机的名字
        map.put("x-dead-letter-exchange", EXCHNAGE_DELAY);
        //死信交换机把消息个哪个个routingkey
        map.put("x-dead-letter-routing-key", ROUTINGKEY_QUEUE_DELAY);
        //队列过期时间10s
        map.put("x-message-ttl", 20000);
        return new Queue(QUEUE_UNKNOW,true,false,false,map);
    }

    @Bean
    public Binding queueOrderBinding(){
        return BindingBuilder.bind(queueOrder()).to(exchangeDelay()).with(ROUTINGKEY_QUEUE_ORDER).noargs();
    }
    @Bean
    public Binding queueDelayBinding(){
        return BindingBuilder.bind(queueDelay()).to(exchangeDelay()).with(ROUTINGKEY_QUEUE_DELAY).noargs();
    }
    @Bean
    public Binding queuePayBinding(){
        return BindingBuilder.bind(queuePay()).to(exchangeDelay()).with(ROUTINGKEY_QUEUE_PAY).noargs();
    }
    @Bean
    public Binding queueUnKnowBinding(){
        return BindingBuilder.bind(queueUnKnow()).to(exchangeDelay()).with(ROUTINGKEY_QUEUE_UNKNOW).noargs();
    }
    @Bean
    public MessageConverter messageConverter(){
        return new Jackson2JsonMessageConverter();
    }
}