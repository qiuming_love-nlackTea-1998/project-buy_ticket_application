package com.qiuming.alibaba.mapper;

import com.qiuming.alibaba.domain.UserTicketCode;

import java.util.List;

/**
 * @author 刘霖枫
 */
public interface UserTicketCodeMapper {

    List<UserTicketCode> getUserTicketCodeByUserId(Long id);

    void saveUserTicketCode(UserTicketCode ticketCode);
}
