package com.qiuming.alibaba.service;

import com.qiuming.alibaba.domain.Order;
import com.qiuming.alibaba.domain.UserTicketCode;

import java.util.List;

/**
 * @author 刘霖枫
 */
public interface OrderService {

    String buyTicket(Order order);

    String payMoney(Order order);

    Order getOrderBySnOrderSn(String orderSn);

    List<UserTicketCode> listUserTicket(Long id);

}
