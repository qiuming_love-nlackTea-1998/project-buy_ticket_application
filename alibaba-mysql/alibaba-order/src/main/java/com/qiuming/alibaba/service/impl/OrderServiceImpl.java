package com.qiuming.alibaba.service.impl;

import com.qiuming.alibaba.api.TicketService;
import com.qiuming.alibaba.api.UserService;
import com.qiuming.alibaba.domain.Order;
import com.qiuming.alibaba.domain.Ticket;
import com.qiuming.alibaba.domain.User;
import com.qiuming.alibaba.domain.UserTicketCode;
import com.qiuming.alibaba.exception.CatchException;
import com.qiuming.alibaba.mapper.OrderMapper;
import com.qiuming.alibaba.mapper.UserTicketCodeMapper;
import com.qiuming.alibaba.service.OrderService;
import com.qiuming.alibaba.util.AjaxResult;
import com.qiuming.alibaba.util.OrderUtils;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import java.math.BigDecimal;
import java.util.*;
import static com.qiuming.alibaba.config.MQConfig.*;

/**
 * @author qiuming
 * @date 2021/03/17 16:18
 **/
@Service
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderMapper orderMapper;

    @Reference
    private TicketService ticketService;

    @Autowired
    private UserTicketCodeMapper userTicketCodeMapper;

    @Reference
    private UserService userService;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    /**
     * 下单方法
     * @param order
     * @return
     */
    @Override
    @Transactional
    public String buyTicket(Order order) {
        // 判断账户是否正确
        User user = order.getUser();
        AjaxResult result = userService.checkAccount(user);
        if (!result.isSuccess() || (Integer)result.getResultObj() < 1) {
            throw new CatchException("账户存在异常，请重新登录后再购买");
        }
        // 判断影票状态
        Long id = order.getTicket().getId();
        Ticket ticket = ticketService.getByTicketId(id);
        if (ticket.getUpTime() == null) {
            throw new CatchException("电影还未上线，无法下单");
        }
        if (ticket.getTicketNum() <= 0) {
            throw new CatchException("影票已售空，无法下单");
        }
        if (ticket.getTicketNum() < order.getBuyNum()) {
            throw new CatchException("影票数量不足，无法下单");
        }

        // 判断用户余额是否充足
        BigDecimal ticketPrice = ticket.getTicketPrice();
        Integer buyNum = order.getBuyNum();
        BigDecimal allPrice = ticketPrice.multiply(new BigDecimal(buyNum));

        user = userService.getUserByUsername(user.getUsername());
        BigDecimal userBalance = user.getBalance();
        if (allPrice.compareTo(userBalance) > 0) {
            throw new CatchException("用户余额不足，请先充值");
        }

        // 创建订单
        Order saveOrder = new Order();
        String orderSn = OrderUtils.getOrderCode(Integer.valueOf(user.getId().toString()));
        saveOrder.setOrderSn(orderSn);
        saveOrder.setBuyNum(order.getBuyNum());
        saveOrder.setPayMoney(allPrice);
        saveOrder.setTicket(ticket);
        saveOrder.setCreateTime(new Date());
        saveOrder.setType(0);
        saveOrder.setUser(user);
        //orderMapper.saveOrder(saveOrder);

        //发送消息到订单队列
        rabbitTemplate.convertAndSend(EXCHNAGE_DELAY,ROUTINGKEY_QUEUE_ORDER,saveOrder);

        //影票库存减少,保存到数据库中
        ticket.setTicketNum(ticket.getTicketNum() - buyNum);
        ticketService.updateTicket(ticket);

        //返回给前台订单号用于支付
        return orderSn;
    }

    /**
     * 支付方法
     * @param getOrder
     * @return
     */
    @Override
    @Transactional
    public String payMoney(Order getOrder) {
        // 验证登陆
        User checkUser = getOrder.getUser();
        AjaxResult result = userService.checkAccount(checkUser);
        if (!result.isSuccess() || (Integer)result.getResultObj() < 1) {
            throw new CatchException("账户存在异常，请重新登录后再购买");
        }

        // 查询订单信息
        Order order = orderMapper.getOrderBySnOrderSn(getOrder.getOrderSn());
        if (order == null) {
            throw new CatchException("订单不存在");
        }

        // 判断是否完成支付
        if (order.getType() == 1) {
            throw new CatchException("订单已完成支付");
        }
        if (order.getType() == -1) {
            throw new CatchException("超出支付时间，订单已取消");
        }

        // 判断用户余额是否充足
        BigDecimal payMoney = order.getPayMoney();
        User user = userService.getUserByUsername(checkUser.getUsername());
        BigDecimal balance = user.getBalance();
        if (payMoney.compareTo(balance) > 0) {
            throw new CatchException("用户余额不足，请先充值");
        }

        // 扣除余额
        balance = balance.subtract(payMoney);
        user.setBalance(balance);
        user.setUsername(null);
        user.setPassword(null);
        userService.updateUser(user);

        // 修改订单状态
        order.setType(1);
        order.setUpdateTime(new Date());
        orderMapper.updateOrder(order);

        // 生成验证码
        String s = UUID.randomUUID().toString();
        String[] split = s.split("-");
        String code = split[2];

        // 保存验证码到数据库
        UserTicketCode ticketCode = new UserTicketCode();
        ticketCode.setUser(user);
        ticketCode.setCode(code);
        ticketCode.setType(0);
        ticketCode.setOrder(order);
        userTicketCodeMapper.saveUserTicketCode(ticketCode);
        //将数据发送到支付队列中进行保存
        rabbitTemplate.convertAndSend(EXCHNAGE_DELAY,ROUTINGKEY_QUEUE_PAY,order);
        return code;
    }

    /**
     * 根据订单编号查询订单详情
     * @param orderSn
     * @return
     */
    @Override
    public Order getOrderBySnOrderSn(String orderSn) {
        return orderMapper.getOrderBySnOrderSn(orderSn);
    }

    @Override
    public List<UserTicketCode> listUserTicket(Long id) {
        List<UserTicketCode> lists = userTicketCodeMapper.getUserTicketCodeByUserId(id);

        // 搜索出所有的上线的电影
        List<Ticket> tickets = ticketService.listAllOnlineTicket();

        // 转换为map的形式 <key,value> -> <id,ticket>
        Map<Long, Ticket> ticketsMap = new HashMap<>(8);
        tickets.forEach(ticket -> {
            ticketsMap.put(ticket.getId(), ticket);
        });

        //补全查询的验证码 信息
        lists.forEach(orderCode -> {
            Long ticketId = orderCode.getOrder().getTicket().getId();
            orderCode.getOrder().setTicket(ticketsMap.get(ticketId));
        });

        return lists;
    }

}
