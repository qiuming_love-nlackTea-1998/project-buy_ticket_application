package com.qiuming.alibaba.mapper;

import com.qiuming.alibaba.domain.Order;

/**
 * @author 刘霖枫
 */
public interface OrderMapper {
    void saveOrder(Order saveOrder);

    Order getOrderBySnOrderSn(String orderSn);

    void updateOrder(Order order);



}
