package com.qiuming.alibaba.controller;

import com.qiuming.alibaba.domain.Order;
import com.qiuming.alibaba.domain.UserTicketCode;
import com.qiuming.alibaba.service.OrderService;
import com.qiuming.alibaba.util.AjaxResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author qiuming
 * @date 2021/03/17 15:56
 **/
@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private OrderService orderServiceImpl;

    /**
     * 下单接口
     * @param order
     * @return 返回订单编号
     */
    @PostMapping("/buyTicket")
    public AjaxResult buyTicket(@RequestBody Order order) {
        String orderSn = orderServiceImpl.buyTicket(order);
        return AjaxResult.me().setResultObj(orderSn);
    }

    /**
     * 支付接口
     * @return 取票码
     */
    @PostMapping("/payMoney")
    public AjaxResult payMoney(@RequestBody Order order) {
        String code = orderServiceImpl.payMoney(order);
        return AjaxResult.me().setResultObj(code);
    }

    /**
     * 根据订单编号查询订单接口
     * @param OrderSn
     * @return 使用订单编号查询订单
     */
    @GetMapping("/getOrderBySnOrderSn/{orderSn}")
    public AjaxResult getOrderBySnOrderSn(@PathVariable("orderSn") String OrderSn) {
        Order order = orderServiceImpl.getOrderBySnOrderSn(OrderSn);
        return AjaxResult.me().setResultObj(order);
    }

    /**
     * 根据用户id查询用户购买的电影票及其去票码
     * @param id
     * @return
     */
    @GetMapping("/listUserTicket/{userId}")
    public AjaxResult listUserTicket(@PathVariable("userId") Long id) {
        List<UserTicketCode> list = orderServiceImpl.listUserTicket(id);
        return AjaxResult.me().setResultObj(list);
    }



}
