package com.qiuming.alibaba.consumer;

import com.qiuming.alibaba.api.TicketService;
import com.qiuming.alibaba.domain.Order;
import com.qiuming.alibaba.domain.to.TicketTO;
import com.qiuming.alibaba.mapper.ConsumerMapper;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.util.Date;

import static com.qiuming.alibaba.config.MQConfig.*;

/**
 * 消费者，监听队列
 * 
 * @author qiuming
 * @date 2021/03/23 10:13
 **/
@Component
public class Consumer {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private ConsumerMapper consumerMapper;

    @Reference
    private TicketService ticketService;

    /**
     * 死信队列，未完成支付进入
     * @param message
     */
    @RabbitListener(queues = QUEUE_DELAY)
    public void delayQueueListener(@Payload String orderSn, Message message) {
        // 从数据库中查询数据，看订单是否支付完成
        Order order = consumerMapper.getOrderBySnOrderSn(orderSn);
        // 订单在数据库中未完成的情况下，将订单修改为未完成状态
        if (order.getType() == 0) {
            //将订单修改为-1，超出支付时间
            order.setUpdateTime(new Date());
            order.setType(-1);
            //保存到数据库
            consumerMapper.updateOrder(order);

            //退回影票数量
            TicketTO ticketTo = new TicketTO();
            Long ticketId = order.getTicket().getId();
            ticketTo.setId(ticketId);
            //设置影票新增的数量
            ticketTo.setBackTicketNum(order.getBuyNum());
            //调用微服务接口,增加影票数量
            ticketService.backTicketNumber(ticketTo);
        }
    }

    /**
     * 生成订单队列
     * @param message
     */
    @RabbitListener(queues = QUEUE_ORDER)
    public void listener2(@Payload Order order, Message message) {
        // 将订单信息保存到数据库
        consumerMapper.saveOrder(order);
        // 设置20s的支付时间，未完成支付则计入死信队列，将订单编号传入死信队列，进行处理
        rabbitTemplate.convertAndSend(EXCHNAGE_DELAY, ROUTINGKEY_QUEUE_UNKNOW, order.getOrderSn());
    }

    /**
     * 支付订单队列
     */
    @RabbitListener(queues = QUEUE_PAY)
    public void listener3(@Payload Order order, Message message) {
        consumerMapper.updateOrder(order);
    }

}
