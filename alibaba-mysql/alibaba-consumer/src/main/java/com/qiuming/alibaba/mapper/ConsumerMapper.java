package com.qiuming.alibaba.mapper;

import com.qiuming.alibaba.domain.Order;

/**
 * 消费者持久层
 */
public interface ConsumerMapper {

    void saveOrder(Order saveOrder);

    void updateOrder(Order order);

    Order getOrderBySnOrderSn(String orderSn);

}
