package com.qiuming.alibaba.domain;

import lombok.Data;

/**
 * 用户的电影票验证码
 * @author qiuming
 * @date 2021/03/18 09:40
 **/
@Data
public class UserTicketCode {
    private Long id;
    private User user;
    private Order order;
    private String code;
    private Integer type;
}
