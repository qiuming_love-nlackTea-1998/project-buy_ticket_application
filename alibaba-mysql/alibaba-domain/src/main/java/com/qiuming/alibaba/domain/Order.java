package com.qiuming.alibaba.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 订单类
 * @author 刘霖枫
 */
@Data
public class Order implements Serializable {
    //集合id
    private Long id;
    //订单编号
    private String orderSn;
    //购买数量
    private Integer buyNum;
    //支付金额
    private BigDecimal payMoney;
    //电影票id
    private Ticket ticket;
    //订单创建时间
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;
    //订单结束时间(支付/取消)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTime;
    //订单类型 0.下单成功；1.支付成功；2.支付取消
    private Integer type;
    //支付的用户
    private User user;
}
