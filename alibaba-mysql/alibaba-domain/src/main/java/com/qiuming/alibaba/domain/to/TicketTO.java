package com.qiuming.alibaba.domain.to;

import java.io.Serializable;

/**
 * <p>
 * 电影票支付失败传输对象
 * </p>
 * 
 * @author qiuming
 * @since 2021-03-30
 **/
public class TicketTO implements Serializable {

    //影票主键id
    private Long id;
    //支付失败返回的影票数量
    private Integer backTicketNum;

    @Override
    public String toString() {
        return "TicketTO{" +
                "id=" + id +
                ", backTicketNum=" + backTicketNum +
                '}';
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getBackTicketNum() {
        return backTicketNum;
    }

    public void setBackTicketNum(Integer backTicketNum) {
        this.backTicketNum = backTicketNum;
    }
}
