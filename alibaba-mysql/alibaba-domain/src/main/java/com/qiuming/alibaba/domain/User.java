package com.qiuming.alibaba.domain;

/**
 * @author 刘霖枫
 */

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 用户/平台人员账号
 * 
 * @author qiuming
 * @date 2021/03/17 16:13
 **/
@Data
public class User implements Serializable {
    private Long id;
    private String username;
    private String password;
    private BigDecimal balance;

}
