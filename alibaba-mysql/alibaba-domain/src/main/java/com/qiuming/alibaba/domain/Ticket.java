package com.qiuming.alibaba.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author qiuming
 * @date 2021/03/17 14:30
 * @param
 * @return
 */
public class Ticket implements Serializable {

    // 电影票id
    private Long id;
    // 电影名
    @NotEmpty(message = "请输入电影名")
    private String name;
    // 上线时间
    private Date upTime;
    // 下线时间
    private Date downTime;
    // 主演
    @NotEmpty(message = "请输入主演姓名")
    private String actor;
    // 剩余数量
    @NotNull(message = "请输入影票数量")
    private Integer ticketNum;
    // 票价
    @NotNull(message = "请输入影票价格")
    private BigDecimal ticketPrice;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    public Date getUpTime() {
        return upTime;
    }

    public void setUpTime(Date upTime) {
        this.upTime = upTime;
    }

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    public Date getDownTime() {
        return downTime;
    }

    public void setDownTime(Date downTime) {
        this.downTime = downTime;
    }

    public String getActor() {
        return actor;
    }

    public void setActor(String actor) {
        this.actor = actor;
    }

    public Integer getTicketNum() {
        return ticketNum;
    }

    public void setTicketNum(Integer ticketNum) {
        this.ticketNum = ticketNum;
    }

    public BigDecimal getTicketPrice() {
        return ticketPrice;
    }

    public void setTicketPrice(BigDecimal ticketPrice) {
        this.ticketPrice = ticketPrice;
    }

    @Override
    public String toString() {
        return "Ticket{" + "id=" + id + ", name='" + name + '\'' + ", upTime=" + upTime + ", downTime=" + downTime
            + ", actor='" + actor + '\'' + ", ticketNum=" + ticketNum + ", ticketPrice=" + ticketPrice + '}';
    }
}
