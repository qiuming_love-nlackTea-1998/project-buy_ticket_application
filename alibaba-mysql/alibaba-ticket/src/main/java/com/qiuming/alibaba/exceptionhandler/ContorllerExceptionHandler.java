package com.qiuming.alibaba.exceptionhandler;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.qiuming.alibaba.util.AjaxResult;

/**
 * @author 刘霖枫
 */
@RestControllerAdvice
public class ContorllerExceptionHandler {

    @ExceptionHandler(Exception.class)
    public AjaxResult globalExceptionCatch(Exception e) {
        e.printStackTrace();
        return AjaxResult.me().setSuccess(false).setMessage("服务器断开，正在殴打程序员");
    }

    @ExceptionHandler(RuntimeException.class)
    public AjaxResult runtimeExceptionCatch(RuntimeException e){
        e.printStackTrace();
        return AjaxResult.me().setSuccess(false).setMessage(e.getMessage());
    }
}
