package com.qiuming.alibaba.service;

import com.qiuming.alibaba.domain.Ticket;

import java.util.List;

/**
 * @author 刘霖枫
 */
public interface TicketService {

    List<Ticket> listAllTicket();

    Ticket getByTicketId(Long id) throws Exception;

    void saveTicket(Ticket ticket);

    void removeTicket(Long id);

    void onlineMovie(Long id);

    void offlineTicket(Long id);

    void updateTicket(Ticket ticket);
}
