package com.qiuming.alibaba.mapper;

import com.qiuming.alibaba.domain.Ticket;

import java.util.List;

/**
 * @author 刘霖枫
 */
public interface TicketMapper {

    /**
     * 查询所有影票详情
     * @return
     */
    List<Ticket> listAllOnlineTicket();

    /**
     * 查询单个电影票详情
     * @param id
     * @return
     */
    Ticket getByTicketId(Long id);

    void saveTicket(Ticket ticket);

    void updateTicket(Ticket ticket);

    void removeTicket(Long id);

    List<String> listAllTicketName();

    List<Long> listAllTicketId();
}
