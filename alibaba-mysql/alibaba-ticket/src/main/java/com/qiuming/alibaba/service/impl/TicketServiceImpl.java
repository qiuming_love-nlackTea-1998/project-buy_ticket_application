package com.qiuming.alibaba.service.impl;

import com.qiuming.alibaba.domain.Ticket;
import com.qiuming.alibaba.exception.CatchException;
import com.qiuming.alibaba.mapper.TicketMapper;
import com.qiuming.alibaba.service.TicketService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * @author 刘霖枫
 */
@Service
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
@CacheConfig(cacheNames = "ticket")
public class TicketServiceImpl implements TicketService {

    @Autowired
    private TicketMapper ticketMapper;

    @Autowired
    private RedisTemplate<Object, Object> redisTemplate;

    /**
     * 查询所有影票详情
     * 
     * @return
     */
    @Override
    @Cacheable(key = "'allOnlineTicket'")
    public List<Ticket> listAllTicket() {
        List<Ticket> tickets = ticketMapper.listAllOnlineTicket();
        return tickets;
    }

    /**
     * 查询单个电影票详情
     * 
     * @param id
     * @return
     */
    @Override
    @Cacheable(key = "#id")
    public Ticket getByTicketId(Long id) throws Exception {
        Ticket ticket = ticketMapper.getByTicketId(id);
        if (ticket == null) {
            throw new CatchException("电影不存在，请刷新后重新查询");
        }
        Date upTime = ticket.getUpTime();
        if (upTime == null) {
            throw new CatchException("电影暂未上线，请耐心等待");
        }
        return ticket;
    }

    /**
     * 新增影票信息方法
     * 
     * @author qiuming
     * @param ticket
     * @return void
     */
    @Override
    @Transactional
    public void saveTicket(Ticket ticket) {
        // 校验信息是否填写完成 注解已完成

        // 校验电影名是否重复
        List<String> tickets = ticketMapper.listAllTicketName();
        boolean contains = tickets.contains(ticket.getName());
        if (contains) {
            throw new CatchException("请不要重复提交");
        }

        // 创建放入数据库的对象
        Ticket saveTicket = new Ticket();
        BeanUtils.copyProperties(ticket, saveTicket);
        saveTicket.setDownTime(null);
        saveTicket.setUpTime(null);

        // 添加数据保存到数据库
        ticketMapper.saveTicket(saveTicket);
    }

    /**
     * 修改影票信息方法
     * @param getTicket
     */
    @Override
    @Caching(evict = {@CacheEvict(key = "#getTicket.id"), @CacheEvict(key = "'allOnlineTicket'")})
    public void updateTicket(Ticket getTicket) {
        // 不可直接使用传入对象进行修改数据库
        Ticket ticket = new Ticket();
        BeanUtils.copyProperties(getTicket, ticket);
        // 修改后需要进入下架状态
        ticket.setUpTime(null);
        ticket.setDownTime(new Date());
        ticketMapper.updateTicket(ticket);
    }

    /**
     * 删除电影票接口
     * @param id
     */
    @Override
    @Transactional
    @Caching(evict = {@CacheEvict(key = "#id"), @CacheEvict(key = "'allOnlineTicket'")})
    public void removeTicket(Long id) {
        List<Long> ids = ticketMapper.listAllTicketId();
        if (!ids.contains(id)) {
            throw new CatchException("电影已删除，请不要重复操作");
        }
        ticketMapper.removeTicket(id);
    }

    /**
     * 上线电影票接口
     * 
     * @param id
     */
    @Override
    @Transactional
    @CacheEvict(key = "'allOnlineTicket'")
    public void onlineMovie(Long id) {
        Ticket ticket = ticketMapper.getByTicketId(id);
        // 判断电影是否存在
        if (ticket == null) {
            throw new CatchException("电影不存在，请重新选择");
        }
        // 判断电影是否已上线
        if (ticket.getUpTime() != null) {
            throw new CatchException("电影已上线，不要重复操作");
        }
        ticket.setUpTime(new Date());
        ticket.setDownTime(null);
        ticketMapper.updateTicket(ticket);
    }

    /**
     * 下线电影票接口
     * 
     * @param id
     */
    @Override
    @Transactional
    @CacheEvict(key = "'allOnlineTicket'")
    public void offlineTicket(Long id) {
        Ticket ticket = ticketMapper.getByTicketId(id);
        // 判断电影是否存在
        if (ticket == null) {
            throw new CatchException("电影不存在，请重新选择");
        }
        // 判断电影是否已下线
        if (ticket.getDownTime() != null) {
            throw new CatchException("电影已下线，不要重复操作");
        }
        ticket.setDownTime(new Date());
        ticket.setUpTime(null);
        ticketMapper.updateTicket(ticket);
    }
}
