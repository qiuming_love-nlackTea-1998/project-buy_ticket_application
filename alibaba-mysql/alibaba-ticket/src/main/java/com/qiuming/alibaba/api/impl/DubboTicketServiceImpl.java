package com.qiuming.alibaba.api.impl;

import com.qiuming.alibaba.domain.to.TicketTO;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import com.qiuming.alibaba.api.TicketService;
import com.qiuming.alibaba.domain.Ticket;
import com.qiuming.alibaba.mapper.TicketMapper;
import java.util.List;

/**
 * dubbo api的实现
 * @author qiuming
 * @date 2021/03/17 19:27
 **/
@Service
public class DubboTicketServiceImpl implements TicketService {

    @Autowired
    private TicketMapper ticketMapper;


    @Override
    public Ticket getByTicketId(Long id) {
        return ticketMapper.getByTicketId(id);
    }

    @Override
    public List<Ticket> listAllOnlineTicket() {
        return ticketMapper.listAllOnlineTicket();
    }

    @Override
    public void updateTicket(Ticket ticket) {
        ticketMapper.updateTicket(ticket);
    }

    @Override
    public void backTicketNumber(TicketTO ticketTo) {
        System.out.println(ticketTo);
        Ticket ticket = ticketMapper.getByTicketId(ticketTo.getId());
        ticket.setTicketNum(ticket.getTicketNum()+ticketTo.getBackTicketNum());
        System.out.println(ticket);
        ticketMapper.updateTicket(ticket);
    }
}
