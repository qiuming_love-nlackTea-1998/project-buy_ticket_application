package com.qiuming.alibaba.controller;

import com.qiuming.alibaba.domain.Ticket;
import com.qiuming.alibaba.service.TicketService;
import com.qiuming.alibaba.util.AjaxResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * 电影票控制层
 * 
 * @author 刘霖枫
 */
@RestController
@RequestMapping("/ticket")
public class TicketController {

    @Autowired
    private TicketService ticketService;

    /**
     * 查询所有电影接口
     * @return 影票详情JSON格式
     */
    @GetMapping("/listAll")
    public List<Ticket> listAllTicket() {
        return ticketService.listAllTicket();
    }

    /**
     * 查询单个电影接口
     */
    @GetMapping("/getByTicketId/{id}")
    public AjaxResult getByTicketId(@PathVariable("id") Long id) throws Exception {
        Ticket ticket = ticketService.getByTicketId(id);
        return AjaxResult.me().setResultObj(ticket);
    }

    /**
     * 新增/修改电影票详情接口
     * @param ticket
     * @return
     */
    @PostMapping("/saveTicket")
    public AjaxResult saveTicket(@RequestBody @Valid Ticket ticket) {
        boolean save = ticket.getId() == null ? true : false;
        if (save) {
            ticketService.saveTicket(ticket);
        } else {
            ticketService.updateTicket(ticket);
        }
        return AjaxResult.me();
    }

    /**
     * 删除电影票接口
     * @param id
     * @return
     */
    @GetMapping("/removeTicket/{id}")
    public AjaxResult removeTicket(@PathVariable("id") Long id) {
        ticketService.removeTicket(id);
        return AjaxResult.me();
    }

    /**
     * 上线电影票接口
     * @param id
     * @return
     */
    @GetMapping("/onlineTicket/{id}")
    public AjaxResult onlineMovie(@PathVariable("id") Long id) {
        ticketService.onlineMovie(id);
        return AjaxResult.me();
    }

    /**
     * 下线电影票接口
     * @param id
     * @return
     */
    @GetMapping("/offlineTicket/{id}")
    public AjaxResult offlineTicket(@PathVariable("id") Long id) {
        ticketService.offlineTicket(id);
        return AjaxResult.me();
    }

}
