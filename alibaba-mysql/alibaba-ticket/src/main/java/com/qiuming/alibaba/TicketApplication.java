package com.qiuming.alibaba;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * @author 刘霖枫
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableCaching
public class TicketApplication {
    public static void main(String[] args) {

        ConfigurableApplicationContext context = SpringApplication.run(TicketApplication.class);

    }
}
