package com.qiuming.alibaba.exception;

/**
 * 抓取的项目异常
 * @author qiuming
 * @date 2021/03/23 09:59
 **/
public class CatchException extends RuntimeException{


    public CatchException(String message) {
        super(message);
    }
}
