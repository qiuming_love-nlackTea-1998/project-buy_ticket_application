package com.qiuming.alibaba.service;

import com.qiuming.alibaba.domain.User;


/**
 * @author 刘霖枫
 */

public interface UserService {
    Integer checkAccount(User user);
}
