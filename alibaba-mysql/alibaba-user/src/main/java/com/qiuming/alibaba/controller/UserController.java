package com.qiuming.alibaba.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.qiuming.alibaba.domain.User;
import com.qiuming.alibaba.service.UserService;
import com.qiuming.alibaba.util.AjaxResult;

/**
 * 用户系统控制层
 * @author qiuming
 * @date 2021/03/19 14:43
 **/

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    public AjaxResult checkAccount(User user){
        Integer access= userService.checkAccount(user);
        return AjaxResult.me().setResultObj(access);
    }

}

