package com.qiuming.alibaba.api.impl;

/**
 * @author 刘霖枫
 */
import com.qiuming.alibaba.api.UserService;
import com.qiuming.alibaba.domain.User;
import com.qiuming.alibaba.mapper.UserMapper;
import com.qiuming.alibaba.util.AjaxResult;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * dubbo api的实现
 * @author qiuming
 * @date 2021/03/17 19:27
 **/
@Service
public class DubboUserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public AjaxResult checkAccount(User user) {
        Integer account = userMapper.checkAccount(user);
        return AjaxResult.me().setResultObj(account);
    }

    @Override
    public User getUserByUsername(String username) {
        return userMapper.getUserByUsername(username);
    }

    @Override
    public void updateUser(User user) {
        userMapper.updateUser(user);
    }
}
