package com.qiuming.alibaba.service.impl;

import org.springframework.beans.factory.annotation.Autowired;

import com.qiuming.alibaba.domain.User;
import com.qiuming.alibaba.mapper.UserMapper;
import com.qiuming.alibaba.service.UserService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author qiuming
 * @date 2021/03/19 14:47
 **/
@Service
@Transactional(readOnly = true,propagation = Propagation.SUPPORTS)
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public Integer checkAccount(User user) {
        return userMapper.checkAccount(user);
    }
}
