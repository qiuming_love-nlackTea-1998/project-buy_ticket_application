package com.qiuming.alibaba.mapper;

import com.qiuming.alibaba.domain.User;

/**
 * @author 刘霖枫
 */
public interface UserMapper {
    Integer checkAccount(User user);

    User getUserByUsername(String username);

    void updateUser(User user);
}
