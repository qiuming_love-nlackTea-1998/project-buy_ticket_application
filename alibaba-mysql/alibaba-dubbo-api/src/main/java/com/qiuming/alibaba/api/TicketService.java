package com.qiuming.alibaba.api;

import com.qiuming.alibaba.domain.Ticket;
import com.qiuming.alibaba.domain.to.TicketTO;

import java.util.List;

/**
 * @author 刘霖枫
 */
public interface TicketService {

    /**
     * dubbo提供的tpc协议，实现微服务之间相互调用
     * 
     * @param id
     * @return
     */
    Ticket getByTicketId(Long id);

    /**
     * 展示所有上线的电影票
     * 
     * @return
     */
    List<Ticket> listAllOnlineTicket();

    /**
     * 修改影票信息
     *
     * @param ticket
     */
    public void updateTicket(Ticket ticket);

    /**
     * 支付失败，影票数量退回接口
     *
     * @param ticketTo 影票传输对象
     * @return
     */
    public void backTicketNumber(TicketTO ticketTo);
}
