package com.qiuming.alibaba.api;


import com.qiuming.alibaba.domain.User;
import com.qiuming.alibaba.util.AjaxResult;

/**
 * @author 刘霖枫
 */
public interface UserService {

    /**
     * dubbo提供的tpc协议，实现微服务之间相互调用
     * @param user
     * @return
     */
    AjaxResult checkAccount(User user);

    User getUserByUsername(String username);

    void updateUser(User user);
}
